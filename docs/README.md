# Progetto - Gpu Computing

Il progetto realizza una versione estesa a tutte le immagini del calcolo delle HOG (Histogram of oriented gradients) utilizzato per il riconoscimento di oggetti all'interno di immagini. 
In particolare il codice si mostra in una versione sequenziale ed una sfruttante l'accellerazione hardware.
Si mostrano e si confrontano infine le efficienze delle due versioni.

## Limitazioni
Il programma funziona solamente con immagini in jpg. In caso di immagini .png / .bmp / ... è necessario convertire le immagini prima di utilizzarle. 

Il programma accetta immagini con risoluzione massima di 1200 x 1600, in formato RGB

## Compilazione

Per compilare il progetto entrare nella cartella /src ed eseguire il seguente comando 

```bash
nvcc main.cu
```

## Utilizzo
Per eseguire il progetto entrare nella cartella /src ed eseguire il seguente comando 
```bash
./a.out
```
Se si vuole utilizzare un'altra immagine rispetto quella di default si può scegliere tra quelle presenti nella cartella /src/images usando il comando
```bash
./a.out images/"nome_immagine_scelta.jpg"
```
Se si vuole utilizzare un'immagine a propria scelta usare il comando
```bash
./a.out "path_assoluto"
```

## Risultati
Tutti i risultati legati alle prestazioni del programma sono visibili da terminale dopo l'esecuzione. E' possibile visionare anche il risultato dei singoli algoritmi sull'immagine fino al calcolo della magnitudine andando nella cartella "results".
```bash
/src/images/results
    |__  testGrayScaleCPU.jpg
    |__  testGrayScaleGPU.jpg
    |__  gradientX.jpg
    |__  gradientY.jpg
    |__  gradientX_GPU.jpg
    |__  gradientY_GPU.jpg
    |__  magnitudeResGPU.jpg
```

## License 
Algoritmo

```bash
[http://gpu.di.unimi.it/docs/hog.pdf]
Navneet Dalal and Bill Triggs

INRIA Rhàone-Alps,655 avenuedel'Europe, Montbonnot 38334, France 
{Navneet.Dalal,Bill.Triggs}
@inrialpes.fr,   http://lear.inrialpes.f
```

Codice 

```bash
Morgan Malavasi mat.960552
morgan.malavasi@studenti.unimi.it
```

