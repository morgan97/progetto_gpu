/*
	THE FOLLOWING FILE CONTAINS ALL THE FUNCTION FOR
    - computation of the HOG
*/

#pragma once 

#define DIM 8
#define length_bin 9



//******************************************** CPU ********************************************// 

void hogCPU(unsigned char *data, unsigned char *angle, float *results, int height, int width)
{
    int row = 0;
    int column = 0;
    int block = 0;

    // 2 while move the cursor on the matrix
    // for selecting the correct block of data
    while (row < height)
    {
        while (column < width)
        {
            // internal loop for iterating on the data of the block selected 
            // assert(block == 0);
            for (int i = row; i < row + DIM; i++)
            {
                for (int j = column; j < column + DIM; j++)
                {
                    // check to not exceed from the width and height 
                    // example -> if the row has size = 20 then the third block will be made by 4x4 elements 
                    // because 8 + 8 + 4 = 20
                    // if it is looped on 21 then it is another row 
                    if (i < height && j < width){
                        // get the two values
                        unsigned char mValue = data[i * width + j];
                        unsigned char mAngle = angle[i * width + j];

                        // printf("mValue = %u , mAngle = %u\n", mValue, mAngle);

                        // get the bins
                        int minor = mAngle / 20; // inferior bin
                        int major = minor + 1;   // superior bin

                        // printf("minor = %d , major = %d\n", minor, major);

                        // get the rests
                        float rest_minor = mAngle - minor * 20;
                        float rest_major = major * 20 - mAngle;

                        // check if it is the end of the last been, if true major bin restart from the first 
                        if (minor == DIM)
                            major = 0;

                        // printf("rest_minor = %f , rest_major = %f\n", rest_minor, rest_major);

                        // get values to insert
                        float inf_value = (rest_major / 20) * mValue;
                        float sup_value = (rest_minor / 20) * mValue;

                        // printf("inf_value = %f , sup_value = %f\n", inf_value, sup_value);

                        // insert values in the correct bin
                        // the index is given by the block where the algo is operating multiplied by the length ot the bin (9)
                        results[block * length_bin + minor] += inf_value;
                        results[block * length_bin + major] += sup_value;
                    }
                }
            }
            block += 1;
            // column increases of 8 
            column += DIM;
        }
        // the row is finished so column increases restart from 0 and row is increased of 8
        column = 0;
        row += DIM;
    }
}


//******************************************** GPU ********************************************// 


__global__ void hogGPU (unsigned char *data, unsigned char *angle, float *results, int height, int width){
    // get row and column
    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int column = blockIdx.x * blockDim.x + threadIdx.x;

    if (row >= height || column >= width)
        return;

    // get the two values
    unsigned char mValue = data[row * width + column];
    unsigned char mAngle = angle[row * width + column];

    // printf("mValue = %u , mAngle = %u\n", mValue, mAngle);

    // get the bins where to insert
    int minor = mAngle / 20; // inferior bin
    int major = minor + 1;   // superior bin

    // printf("minor = %d , major = %d\n", minor, major);

    // get the rests
    float rest_minor = mAngle - minor * 20;
    float rest_major = major * 20 - mAngle;

    if (minor == DIM)
        major = 0;

    // printf("rest_minor = %f , rest_major = %f\n", rest_minor, rest_major);

    // get values to insert
    float inf_value = (rest_major / 20) * mValue;
    float sup_value = (rest_minor / 20) * mValue;

    // printf("inf_value = %f , sup_value = %f\n", inf_value, sup_value);

    // insert values in the correct bin
    // for finding the correct place it is necessary to have the block ID 
    // blockID = position of block along x multiplied by the number of block in a row, then + position of block along y 
    // It is the same concept of r * dim + c but instead of using direct values, 
    // are used the blocks 
    int numBlockPerRow = (width + DIM - 1) / DIM;
    int blockID = blockIdx.y * numBlockPerRow + blockIdx.x;
    // because more thread can access the bins at the same time it is used the CUDA function 
    // for creating a lock on the data and make the sum 
    atomicAdd(&results[blockID * length_bin + minor], inf_value);
    atomicAdd(&results[blockID * length_bin + major], sup_value);
    
    //results[blockIdx.x * length_bin + minor] += inf_value;
    //results[blockIdx.x * length_bin + major] += sup_value;

}


