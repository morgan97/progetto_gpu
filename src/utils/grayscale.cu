/*
	THE FOLLOWING FILE CONTAINS ALL THE FUNCTION FOR
	- read / write image 
	- grayscaling operation
*/

#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


/*
	GRAYSCALING
	All the rights reserved to https://www.tutorialspoint.com/dip/grayscale_to_rgb_conversion.htm 
	Grayscale can be optained using two different methods.
	1. Average method 
	Average method is the most simple one. You just have to take the average 
	of three colors. Since its an RGB image, so it means that you have add r with g with b and 
	then divide it by 3 to get desired grayscale image.
	Grayscale = ((R + G + B) / 3)
	The result will be very dark! 
	This problem arise due to the fact, that we take average of the three colors. 
	Since the three different colors have three different wavelength 
	and have their own contribution in the formation of image, 
	so we have to take average according to their contribution, 
	not done it averagely using average method
	2. Weighted method 
	grayscale image = ( (0.3 * R) + (0.59 * G) + (0.11 * B) ).
	According to this equation, Red has contribute 30%, 
	Green has contributed 59% which is greater in all three colors 
	and Blue has contributed 11%.
*/

//******************************************** CPU ********************************************// 

void grayscaleCPU (unsigned char *img, unsigned char *gray_img, const size_t image_size, const int channels, const int gray_channels){
	// loop on all the pixel in the image
	// p = R 
	// p + 1 = G
	// p + 2 = B
	for (unsigned char *p = img, *pg = gray_img; p != img + image_size; p += channels, pg += gray_channels){
		unsigned char grayscale = ((0.3 * *p) + (0.59 * *(p+1)) + (0.11 * *(p+2)));
		*pg = grayscale;
		// cpy the value of also the fourth channel
	}
}


//******************************************** GPU ********************************************// 

__global__ void grayscaleGPU(unsigned char *img, unsigned char *gray_img, const size_t image_size, const int channels, const int gray_channels){
	uint i = blockIdx.x * blockDim.x + threadIdx.x;
	// if the number of is oversize
	if (i >= image_size)
		return;

	// compute stride, RGB jump of 3
	uint idx = i * 3;

	uint idx_gray = i;

	// get values 
	unsigned char rValue = img[idx];
	unsigned char gValue = img[idx+1];
	unsigned char bValue = img[idx+2];
	
	unsigned char grayscale = ((0.3 * rValue) + (0.59 * gValue) + (0.11 * bValue));
	gray_img[idx_gray] = grayscale;
}

