/*
	THE FOLLOWING FILE CONTAINS ALL THE FUNCTION FOR
    - magnitude 
    - arcotangent
*/

#pragma once 

#define PI 3.14159265358979323846

//******************************************** CPU ********************************************// 

void magnitudeCPU (unsigned char *gradientX, unsigned char * gradientY, unsigned char *magnitude, const int size){
	// loop on all the data and get magnitude
	for (int i=0; i<size; i++)
		magnitude[i] = sqrt(pow(gradientX[i], 2) + pow(gradientY[i], 2));
}

void arcotangentCPU (unsigned char *gradientX, unsigned char * gradientY, unsigned char *arcotangent, const int size){
	// loop on all the data and get arcotangent
	for (int i=0; i<size; i++)
		arcotangent[i] = atan2(gradientY[i], gradientX[i]) * (180 / PI);
}


//******************************************** GPU ********************************************// 

__global__ void magnitudeGPU (unsigned char *gradientX, unsigned char *gradientY, unsigned char *magnitude, const int size){
	uint i = blockDim.x * blockIdx.x + threadIdx.x;
	// check to not be outside the range of values
	if (i >= size)
        return;
	magnitude[i] = sqrtf(powf(gradientX[i], 2) + powf(gradientY[i], 2));
}

__global__ void arcotangentGPU (unsigned char *gradientX, unsigned char *gradientY, unsigned char *arcotangent, const int size){
	uint i = blockDim.x * blockIdx.x + threadIdx.x;
    if (i >= size)
        return; 
	arcotangent[i] = atan2f(gradientY[i], gradientX[i]) * (180 / PI);
}