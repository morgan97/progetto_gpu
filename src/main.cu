// Project of GPU_COMPUTING 
// HOG (Histograms of oriented gradients)
/*
    This main file contains all the process of the program 

    select device 
    |
    read image 
    | 
    RGB -> grayscale
    |
    filtering with two masks
    |
    magnitude and arcotangent
    |
    HOG

    All the calls to the functions CPU / GPU are located in the utils directory

    To explain better the results it is possible to see the results of algorithms 
    CPU and GPU version in the src/images/results folder

    The images folder contains a number of images for trying the program 
    ./a.out     images/"name_of_the_image.jpg"
    but it is possible also to test an image using the absolute path 
    ./a.out     absolute path
*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "utils/grayscale.cu"
#include "utils/gradient.cu"
#include "utils/mag_arcot.cu"
#include "utils/hog.cu"
#include "utils/utility.h"
#include "utils/common.h"

#define STB_IMAGE_IMPLEMENTATION 
#include "stb_image/stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION 
#include "stb_image/stb_image_write.h"

#define BLOCKDIM_GRAYSCALING 32
#define BLOCKDIM_CONVOLUTION 32
#define BLOCKDIM_MAGNITUDE_ARCOTANGENT 32
#define BLOCK_HOG 8

#define N_STREAM 4

int main (int argc, char **argv) {

    printf("\n\n");
    //**********************************************//
    // selection of the device                      // 
    //**********************************************//

    int numDevices = 0;
    char *name;
    CHECK(cudaGetDeviceCount(&numDevices));
    if (numDevices > 1){
        printf("Number of Devices = %d\n", numDevices);
        int maxMultiprocessors = 0, maxDevice = 0;
        for (int device = 0; device<numDevices; device++){
            cudaDeviceProp props;
            cudaGetDeviceProperties(&props, device);
            if (maxMultiprocessors < props.multiProcessorCount){
                name = props.name;
                maxMultiprocessors = props.multiProcessorCount;
                maxDevice = device;
            }
        }
        CHECK(cudaSetDevice(maxDevice));
        printf("device = %s \n", name);
    }    

    //**********************************************//
    // read image from images dir or path           //
    //**********************************************//
    // load start image 
    int width, height, channels;
    unsigned char *load;
    if (argc > 1)
        load = stbi_load(argv[1], &width, &height, &channels, 0);
    else
        load = stbi_load("images/test.jpg", &width, &height, &channels, 0);
    if (load == NULL){
        printf("Error loading the image... \n");
        exit(EXIT_FAILURE);
    }
    printf("Loaded image with a width of %dpx, a height of %dpx and %d channels\n\n", width, height, channels);

    assert(check_size(width, height));
    assert(check_channels(channels));

    // size of the original image 
    size_t image_size = width * height * channels * sizeof(unsigned char);
    printf("Data size = %zu\n\n", image_size);
    
    // allocation pinned memory 
    unsigned char *img;
    CHECK(cudaHostAlloc((void**)&img, image_size, cudaHostAllocDefault));
    memcpy(img, load, image_size);


    // utils/grayscale.cu
    //********************************************************************************************//
    //********************************************************************************************//
    // Convert image RGB -> grayscale                                                             // 
    //********************************************************************************************//
    //********************************************************************************************//

    printf("****************** GRAYSCALING ******************\n\n");

    // creation of the events for time management
    cudaEvent_t s_t_a_r_t_, s_t_o_p_;
	CHECK(cudaEventCreate(&s_t_a_r_t_));
    CHECK(cudaEventCreate(&s_t_o_p_));
    float time;
    
    // alloc memory for the gray scaled image
    int gray_channels = 1;         // grayscale img only has 1 channel 
    size_t gray_image_size = width * height * gray_channels * sizeof(unsigned char);
    unsigned char *gray_img;
    CHECK(cudaHostAlloc((void**)&gray_img, gray_image_size, cudaHostAllocDefault));

    if (gray_img == NULL){
        printf("Unable to allocate memory for the gray image.\n");
        exit(EXIT_FAILURE);
    }

    // ************ CPU ************ // 
    CHECK(cudaEventRecord(s_t_a_r_t_, 0));
    grayscaleCPU(img, gray_img, image_size, channels, gray_channels);
    CHECK(cudaEventRecord(s_t_o_p_, 0));
    CHECK(cudaEventSynchronize(s_t_o_p_));
	CHECK(cudaEventElapsedTime(&time, s_t_a_r_t_, s_t_o_p_));
    printf("Time CPU Grayscale = %f ms\n", time);
    stbi_write_jpg("images/results/testGrayScaleCPU.jpg", width, height, gray_channels, gray_img, 100);   // 100 stand for the quality we use to cpy 
    float init = time;  
    
    // ************ GPU ************ // 
    // alloc mem for img in GPU 
    unsigned char *imgGPU;
    CHECK(cudaMalloc((void**)&imgGPU, image_size));
    unsigned char *gray_imgGPU;
    CHECK(cudaMalloc((void**)&gray_imgGPU, gray_image_size));
    
    dim3 block_gray(BLOCKDIM_GRAYSCALING);
    dim3 grid_gray( (width * height + block_gray.x - 1) / block_gray.x );       // width * height is the length of the array 

    CHECK(cudaEventRecord(s_t_a_r_t_, 0));
    CHECK(cudaMemcpy(imgGPU, img, image_size, cudaMemcpyHostToDevice));
    grayscaleGPU<<<grid_gray, block_gray>>>(imgGPU, gray_imgGPU, width * height, channels, gray_channels);    
    CHECK(cudaDeviceSynchronize());
    
    // copy final data 
    unsigned char *gray_img_gpu = (unsigned char*) malloc(gray_image_size);
    CHECK(cudaMemcpy(gray_img_gpu, gray_imgGPU, gray_image_size, cudaMemcpyDeviceToHost));
    CHECK(cudaEventRecord(s_t_o_p_, 0));
    CHECK(cudaEventSynchronize(s_t_o_p_));
	CHECK(cudaEventElapsedTime(&time, s_t_a_r_t_, s_t_o_p_));
    
    // printf speed-up 
    float better = time;                        // better = new time detected 
    float inc = init - better;                  // inc = increment 
    float speedup = inc * 100 / init;           // speed up = speed up in performance in %
    init = time;                                // store time for next computation 
    printf("Time GPU Grayscale = %f ms, speed-up = %f%\n", time, speedup);  

    // ************ GPU stream ***** // 
    // algo with 4 streams
    // alloc mem
    unsigned char *imgGPU_stream;
    CHECK(cudaMalloc((void**)&imgGPU_stream, image_size));
    unsigned char *gray_imgGPU_stream;
    CHECK(cudaMalloc((void**)&gray_imgGPU_stream, gray_image_size));
    unsigned char *gray_img_stream_res;
    CHECK(cudaHostAlloc((void**)&gray_img_stream_res, gray_image_size, cudaHostAllocDefault));

    // maybe data are not divisible by the number of stream

    int n_stream = N_STREAM;
    while ( (width * height) % n_stream != 0) 
        n_stream++;

    // division of the element || 
    //      iElem = dim of 1/4 blocks
    //      iBytes = bytes in one block for the results (because it has 1 channel)
    //      cBytes = bytes in one block for the input (format in RGB)
    int iElem = (width * height) / n_stream;
    size_t iBytes = iElem * gray_channels * sizeof(unsigned char);
    size_t cBytes = iElem * channels * sizeof(unsigned char);
    grid_gray.x = (iElem + block_gray.x - 1) / block_gray.x;

    // creation of 4 non blocking streams 
    cudaStream_t stream[n_stream];

    for (int i=0; i<n_stream; i++)
        CHECK(cudaStreamCreateWithFlags(&stream[i], cudaStreamNonBlocking ));

    
    CHECK(cudaEventRecord(s_t_a_r_t_, 0));
    for (int i=0; i<n_stream; i++){
        // loop over the 4 blocks -> H2D -> algo -> D2H
        int ioffset = i * iElem * 3;
        int grayoffset = i * iElem;
        CHECK(cudaMemcpyAsync(&imgGPU_stream[ioffset], &img[ioffset], cBytes, cudaMemcpyHostToDevice, stream[i]));
        grayscaleGPU<<<grid_gray, block_gray, 0, stream[i]>>>(&imgGPU_stream[ioffset], &gray_imgGPU_stream[grayoffset], iElem, channels, gray_channels);
        CHECK(cudaMemcpyAsync(&gray_img_stream_res[grayoffset], &gray_imgGPU_stream[grayoffset], iBytes, cudaMemcpyDeviceToHost, stream[i]));
    }

    CHECK(cudaEventRecord(s_t_o_p_, 0));
    CHECK(cudaEventSynchronize(s_t_o_p_));
    CHECK(cudaEventElapsedTime(&time, s_t_a_r_t_, s_t_o_p_));
    
    // printf speed-up 
    better = time;
    inc = init - better;
    speedup = inc * 100 / init;
    printf("Time GPU Grayscale with %d Stream= %f ms, speed-up = %f%\n", n_stream, time, speedup); 
    
    for (int i = 0; i < n_stream; ++i) {
		CHECK(cudaStreamDestroy(stream[i]));
	}

    // check results 
    // TODO -> remove the fourth channel
    CHECK(cudaGetLastError());
    
    // check results 
    assert(check(gray_img, gray_img_stream_res, gray_image_size));
    assert(check(gray_img, gray_img_gpu, gray_image_size));
    
    
    stbi_write_jpg("images/results/testGrayScaleGPU.jpg", width, height, gray_channels, gray_img_gpu, 100);
    // stbi_write_jpg("images/results/gray_img_stream_res.jpg", width, height, gray_channels, gray_img_gpu, 100);
    
    printf("\n");



    // utils/gradient.cu
    //********************************************************************************************//
    //********************************************************************************************//
    // Convolution 2D                                                                             // 
    //********************************************************************************************//
    //********************************************************************************************//
    printf("****************** CONVOLUTION ******************\n\n");
    
    // ************ CPU ************ // 
    // alloc mem
    unsigned char* gradientX = (unsigned char*) malloc (gray_image_size);
    unsigned char* gradientY = (unsigned char*) malloc (gray_image_size);
    
    CHECK(cudaEventRecord(s_t_a_r_t_, 0));
    convolution2DH(gray_img, kernel, gradientX, height, width);     // Horizontal gradients
    convolution2DV(gray_img, kernel, gradientY, height, width);     // Vertical gradients
    CHECK(cudaEventRecord(s_t_o_p_, 0));
    CHECK(cudaEventSynchronize(s_t_o_p_));
	CHECK(cudaEventElapsedTime(&time, s_t_a_r_t_, s_t_o_p_));
    
    printf("Time CPU Convolution X and Y = %f ms\n", time);
    init = time;       // save time

    // store images in the images/results folder
    stbi_write_jpg("images/results/gradientX.jpg", width, height, 1, gradientX, 100);
    stbi_write_jpg("images/results/gradientY.jpg", width, height, 1, gradientY, 100);

    
    // ************ GPU ************ // 
    // copy data of gray image
    unsigned char *gray_image_gpu;
    CHECK(cudaMalloc((void**)&gray_image_gpu, gray_image_size));
    
    // alloc memory for the result 
    unsigned char *gradientXResult;
    CHECK(cudaMalloc((void**)&gradientXResult, gray_image_size));
    unsigned char *gradientYResult;
    CHECK(cudaMalloc((void**)&gradientYResult, gray_image_size));
    
    // the final number of block is given by the mult of 
    // the number of block per row and the height of the image 
    int dimBlock = BLOCKDIM_CONVOLUTION;
    int rowBlock = (width + dimBlock - 1) / dimBlock;       // rowblock = block in a row 
    int dimGrid = rowBlock * height;                        // dimgrid = n. of blocks 

    CHECK(cudaEventRecord(s_t_a_r_t_, 0));
    // H2D
    CHECK(cudaMemcpy(gray_image_gpu, gray_img, gray_image_size, cudaMemcpyHostToDevice));
    convolution2DH_GPU<<<dimGrid, dimBlock>>>(gray_image_gpu, gradientXResult, MASK_SIZE, width);
    convolution2DV_GPU<<<dimGrid, dimBlock>>>(gray_image_gpu, gradientYResult, MASK_SIZE, width, height);
    CHECK(cudaDeviceSynchronize());

    // D2H data
    unsigned char *gradientXGPU = (unsigned char*) malloc (gray_image_size);
    unsigned char *gradientYGPU = (unsigned char*) malloc (gray_image_size);

    CHECK(cudaMemcpy(gradientXGPU, gradientXResult, gray_image_size, cudaMemcpyDeviceToHost));
    CHECK(cudaMemcpy(gradientYGPU, gradientYResult, gray_image_size, cudaMemcpyDeviceToHost));

    CHECK(cudaEventRecord(s_t_o_p_, 0));
    CHECK(cudaEventSynchronize(s_t_o_p_));
    CHECK(cudaEventElapsedTime(&time, s_t_a_r_t_, s_t_o_p_));
    
    // printf speed-up 
    better = time;
    inc = init - better;
    speedup = inc * 100 / init;
    init = time;
    printf("Time GPU Convolution X and Y = %f ms, speed-up = %f %\n", time, speedup);


    CHECK(cudaGetLastError());

    // ************ GPU stream + shared mem ****** // 
    
    unsigned char *gray_image_gpu_stream;
    CHECK(cudaMalloc((void**)&gray_image_gpu_stream, gray_image_size));
    unsigned char *gradientXResult_stream;
    CHECK(cudaMalloc((void**)&gradientXResult_stream, gray_image_size));
    unsigned char *gradientYResult_stream;
    CHECK(cudaMalloc((void**)&gradientYResult_stream, gray_image_size));
    
    // parallelization of the gradient X and Y, parallel work on 2 streams 
    cudaStream_t stream1;
    CHECK(cudaStreamCreateWithFlags(&stream1, cudaStreamNonBlocking));

    CHECK(cudaEventRecord(s_t_a_r_t_, 0));
    // H2D
    CHECK(cudaMemcpy(gray_image_gpu_stream, gray_img, gray_image_size, cudaMemcpyHostToDevice));

    convolution2DH_GPU_shared<<<dimGrid, dimBlock>>>(gray_image_gpu_stream, gradientXResult_stream, MASK_SIZE, width);      // this version uses also shared memory 
    convolution2DV_GPU<<<dimGrid, dimBlock, 0, stream1>>>(gray_image_gpu_stream, gradientYResult_stream, MASK_SIZE, width, height);

    // D2H
    unsigned char *gradientXGPU_stream;
    unsigned char *gradientYGPU_stream;
    CHECK(cudaHostAlloc((void**)&gradientXGPU_stream, gray_image_size, cudaHostAllocDefault));
    CHECK(cudaHostAlloc((void**)&gradientYGPU_stream, gray_image_size, cudaHostAllocDefault));
    CHECK(cudaMemcpyAsync(gradientXGPU_stream, gradientXResult_stream, gray_image_size, cudaMemcpyDeviceToHost));
    CHECK(cudaMemcpyAsync(gradientYGPU_stream, gradientYResult_stream, gray_image_size, cudaMemcpyDeviceToHost, stream1));

    CHECK(cudaEventRecord(s_t_o_p_, 0));
    CHECK(cudaEventSynchronize(s_t_o_p_));
    CHECK(cudaEventElapsedTime(&time, s_t_a_r_t_, s_t_o_p_));
    // printf speed-up 
    better = time;
    inc = init - better;
    speedup = inc * 100 / init;
    printf("Time GPU Convolution X and Y with 2 streams and shared mem = %f ms, speed-up = %f %\n", time, speedup);

    cudaStreamDestroy(stream1);

    CHECK(cudaGetLastError());
    
    // check results
    assert(check(gradientX, gradientXGPU, gray_image_size));
    assert(check(gradientY, gradientYGPU, gray_image_size));
    assert(check(gradientX, gradientXGPU_stream, gray_image_size));
    assert(check(gradientY, gradientYGPU_stream, gray_image_size));

    stbi_write_jpg("images/results/gradientX_GPU.jpg", width, height, 1, gradientXGPU, 100);
    stbi_write_jpg("images/results/gradientY_GPU.jpg", width, height, 1, gradientYGPU, 100);

    printf("\n");



    // utils/mag_arcot.cu
    //********************************************************************************************//
    //********************************************************************************************//
    // Magnitude and Tangent                                                                      //
    //********************************************************************************************//
    //********************************************************************************************//
    printf("****************** MAGNITUDE AND ARCOTANGENT ****\n\n");
    size_t size_memory = gray_image_size;
    
    // ************ CPU ************ // 
    // computation of the gradient magnitude on CPU 
    CHECK(cudaEventRecord(s_t_a_r_t_, 0));
    unsigned char* magnitudeResCPU = (unsigned char*) malloc (size_memory);
    magnitudeCPU(gradientX, gradientY, magnitudeResCPU, size_memory);
    // computation of arcotangent
    unsigned char* arcotangentResCPU = (unsigned char*) malloc (size_memory);
    arcotangentCPU(gradientX, gradientY, arcotangentResCPU, size_memory);
    CHECK(cudaEventRecord(s_t_o_p_, 0));
    CHECK(cudaEventSynchronize(s_t_o_p_));
    CHECK(cudaEventElapsedTime(&time, s_t_a_r_t_, s_t_o_p_));
    init = time;
    printf("Time CPU Magnitude and arcotangent X and Y = %f ms\n", time);



    // ************ GPU ************ // 
    // computation of the gradient magnitude on GPU
    unsigned char *magnitudeDataCopy;
    CHECK(cudaMalloc((void**)&magnitudeDataCopy, size_memory));
    unsigned char *arcotangentDataCopy;
    CHECK(cudaMalloc((void**)&arcotangentDataCopy, size_memory));
    
    dim3 block_magnitude_arcotangent(BLOCKDIM_MAGNITUDE_ARCOTANGENT);
    dim3 grid_magnitude_arcotangent( (height * width + block_magnitude_arcotangent.x - 1) / block_magnitude_arcotangent.x );
    
    CHECK(cudaEventRecord(s_t_a_r_t_, 0));
    magnitudeGPU<<<grid_magnitude_arcotangent, block_magnitude_arcotangent>>>(gradientXResult, gradientYResult, magnitudeDataCopy, width * height);
    CHECK(cudaDeviceSynchronize());
    arcotangentGPU<<<grid_magnitude_arcotangent, block_magnitude_arcotangent>>>(gradientXResult, gradientYResult, arcotangentDataCopy, width * height);
    CHECK(cudaDeviceSynchronize());

    // D2H
    unsigned char *magnitudeResGPU = (unsigned char*) malloc (size_memory);
    CHECK(cudaMemcpy(magnitudeResGPU, magnitudeDataCopy, size_memory, cudaMemcpyDeviceToHost));
    unsigned char *arcotangentResGPU = (unsigned char*) malloc (size_memory);
    CHECK(cudaMemcpy(arcotangentResGPU, arcotangentDataCopy, size_memory, cudaMemcpyDeviceToHost));

    CHECK(cudaEventRecord(s_t_o_p_, 0));
    CHECK(cudaEventSynchronize(s_t_o_p_));
    CHECK(cudaEventElapsedTime(&time, s_t_a_r_t_, s_t_o_p_));
    // printf speed-up 
    better = time;
    inc = init - better;
    speedup = inc * 100 / init;
    init = time;
    printf("Time GPU Magnitude and arcotangent X and Y = %f ms, speed-up = %f %\n", time, speedup);

    CHECK(cudaGetLastError());

    // ************ GPU stream ****** //
    // computation of the gradient magnitude on GPU with two streams
    unsigned char *magnitudeDataCopy_stream;
    CHECK(cudaMalloc((void**)&magnitudeDataCopy_stream, size_memory));
    unsigned char *arcotangentDataCopy_stream;
    CHECK(cudaMalloc((void**)&arcotangentDataCopy_stream, size_memory));

    // second strem
    cudaStream_t stream2;
    CHECK(cudaStreamCreateWithFlags(&stream2, cudaStreamNonBlocking));

    CHECK(cudaEventRecord(s_t_a_r_t_, 0));
    magnitudeGPU<<<grid_magnitude_arcotangent, block_magnitude_arcotangent>>>(gradientXResult, gradientYResult, magnitudeDataCopy_stream, width * height);
    arcotangentGPU<<<grid_magnitude_arcotangent, block_magnitude_arcotangent, 0, stream2>>>(gradientXResult, gradientYResult, arcotangentDataCopy_stream, width * height);

    // D2H
    unsigned char *magnitudeResGPU_stream;
    unsigned char *arcotangentResGPU_stream;
    CHECK(cudaHostAlloc((void**)&magnitudeResGPU_stream, size_memory, cudaHostAllocDefault));
    CHECK(cudaHostAlloc((void**)&arcotangentResGPU_stream, size_memory, cudaHostAllocDefault));
    CHECK(cudaMemcpy(magnitudeResGPU_stream, magnitudeDataCopy_stream, size_memory, cudaMemcpyDeviceToHost));
    CHECK(cudaMemcpyAsync(arcotangentResGPU_stream, arcotangentDataCopy_stream, size_memory, cudaMemcpyDeviceToHost, stream2));

    CHECK(cudaEventRecord(s_t_o_p_, 0));
    CHECK(cudaEventSynchronize(s_t_o_p_));
    CHECK(cudaEventElapsedTime(&time, s_t_a_r_t_, s_t_o_p_));
    // printf speed-up 
    better = time;
    inc = init - better;
    speedup = inc * 100 / init;
    printf("Time GPU Magnitude and arcotangent X and Y with 2 streams = %f ms, speed-up = %f %\n", time, speedup);

    cudaStreamDestroy(stream2);

    CHECK(cudaGetLastError());


    assert(check(magnitudeResCPU, magnitudeResGPU, size_memory));
    assert(check(arcotangentResCPU, arcotangentResGPU, size_memory));
    assert(check(magnitudeResCPU, magnitudeResGPU_stream, size_memory));
    assert(check(arcotangentResCPU, arcotangentResGPU_stream, size_memory));

    stbi_write_jpg("images/results/magnitudeResGPU.jpg", width, height, 1, magnitudeResGPU, 100);
    // stbi_write_jpg("images/results/arcotangentResGPU.jpg", width, height, 1, arcotangentResGPU, 100);

    printf("\n");

    // utils/hog.cu
    //********************************************************************************************//
    //********************************************************************************************//
    // HOG                                                                                        //
    //********************************************************************************************//
    //********************************************************************************************//
    printf("****************** HOG **************************\n\n");

    // ************ CPU ************ // 
    int w = (width + BLOCK_HOG - 1) / BLOCK_HOG;            // number of blocks in a row
    int h = (height + BLOCK_HOG - 1) / BLOCK_HOG;           // number of blocks in a column
    float *hogResCPU = (float*) malloc ( w * h  * length_bin * sizeof(float));
    // void *memset(void *str, int c, size_t n)
    memset(hogResCPU, 0.0, w * h  * length_bin * sizeof(float));        // set the bin to zero, if it is avoided bins don't start from zero 
    CHECK(cudaEventRecord(s_t_a_r_t_, 0));
    hogCPU(magnitudeResCPU, arcotangentResCPU, hogResCPU, height, width);
    CHECK(cudaEventRecord(s_t_o_p_, 0));
    CHECK(cudaEventSynchronize(s_t_o_p_));
    CHECK(cudaEventElapsedTime(&time, s_t_a_r_t_, s_t_o_p_));
    init = time;    // save time
    printf("Time CPU Hog = %f ms\n", time);

    // ************ GPU ************ // 
    float *hogCopy;
    CHECK(cudaMalloc((void**)&hogCopy, w * h * length_bin * sizeof(float)));
    CHECK(cudaMemset(hogCopy, 0.0, w * h * length_bin * sizeof(float)));            // the same, set bins to zero

    CHECK(cudaEventRecord(s_t_a_r_t_, 0));
    // H2D
    CHECK(cudaMemcpy(magnitudeDataCopy, magnitudeResCPU, size_memory, cudaMemcpyHostToDevice));
    CHECK(cudaMemcpy(arcotangentDataCopy, arcotangentResCPU, size_memory, cudaMemcpyHostToDevice));
    
    dim3 block_hog(BLOCK_HOG, BLOCK_HOG);
    dim3 grid_hog( (width + block_hog.x - 1) / block_hog.x ,  (height + block_hog.y - 1) / block_hog.y );
    hogGPU<<<grid_hog, block_hog>>>(magnitudeDataCopy, arcotangentDataCopy, hogCopy, height, width);
    CHECK(cudaDeviceSynchronize());

    //D2H
    float *hogResGPU = (float*) malloc (w * h * length_bin * sizeof(float));
    CHECK(cudaMemcpy(hogResGPU, hogCopy, w * h * length_bin * sizeof(float), cudaMemcpyDeviceToHost));
    CHECK(cudaEventRecord(s_t_o_p_, 0));
    CHECK(cudaEventSynchronize(s_t_o_p_));
    CHECK(cudaEventElapsedTime(&time, s_t_a_r_t_, s_t_o_p_));
    
    // printf speed-up 
    better = time;
    inc = init - better;
    speedup = inc * 100 / init;
    init = time;
    
    printf("Time GPU Hog = %f ms, speed-up = %f %\n", time, speedup);

    assert(check(hogResCPU, hogResGPU, w * h * length_bin ));    

    CHECK(cudaGetLastError());

    
    //  dealloc memory
    stbi_image_free(load);
    CHECK(cudaFreeHost(img));
    CHECK(cudaFreeHost(gray_img));
    CHECK(cudaFree(imgGPU));
    CHECK(cudaFree(gray_imgGPU));
    free(gray_img_gpu);
    CHECK(cudaFree(imgGPU_stream));
    CHECK(cudaFree(gray_imgGPU_stream));
    CHECK(cudaFreeHost(gray_img_stream_res));
    free(gradientX);
    free(gradientY);
    CHECK(cudaFree(gray_image_gpu));
    CHECK(cudaFree(gradientXResult));
    CHECK(cudaFree(gradientYResult));
    free(gradientXGPU);
    free(gradientYGPU);
    CHECK(cudaFree(gray_image_gpu_stream));
    CHECK(cudaFree(gradientXResult_stream));
    CHECK(cudaFree(gradientYResult_stream));
    CHECK(cudaFreeHost(gradientXGPU_stream));
    CHECK(cudaFreeHost(gradientYGPU_stream));
    free(magnitudeResCPU);
    free(arcotangentResCPU);
    CHECK(cudaFree(magnitudeDataCopy));
    CHECK(cudaFree(arcotangentDataCopy));
    free(magnitudeResGPU);
    free(arcotangentResGPU);
    CHECK(cudaFree(magnitudeDataCopy_stream));
    CHECK(cudaFree(arcotangentDataCopy_stream));
    CHECK(cudaFreeHost(magnitudeResGPU_stream));
    CHECK(cudaFreeHost(arcotangentResGPU_stream));
    free(hogResCPU);
    CHECK(cudaFree(hogCopy));
    free(hogResGPU);

    printf("\n\n");

    return EXIT_SUCCESS;
}



